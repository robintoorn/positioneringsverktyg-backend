const mongodb = require('mongoose');

const productSchema = mongodb.Schema({
    _id: mongodb.Schema.Types.ObjectId,
    name: { type: String, required: true },
    description: { type: String, required: true },
    samarbetspartner: { type: String, required: true },
    sektion: { type: String, required: true },
    top: { type: String, required: true },
    left: { type: String, required: true },
    productImagesAndVideos: { type: Array, required: true},
})

module.exports = mongodb.model('Product', productSchema);