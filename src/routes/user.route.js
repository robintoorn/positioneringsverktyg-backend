const checkAuth = require('../auth/check-auth');
const route = require('express').Router();

const users = require('../controllers/user.controller.js');

route.post('/signin', users.signIn);
route.put('/:id', checkAuth, users.updatePwd);

module.exports = route;