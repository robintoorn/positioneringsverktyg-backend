const port = process.env.PORT || 8000;
const http = require('http');
const db = require('mongoose');
const app = require('../app');

//Node JS Web Server
http.createServer(app).listen(port, () => { 
    console.log(`NODE JS API Web Server - Online on port ${port}`);
});