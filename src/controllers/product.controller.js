const mongodb = require('mongoose');
const Product = require('../models/product.model');

const fs = require('fs')
const { promisify } = require('util')

const deleteFile = promisify(fs.unlink)

exports.getProducts = (req, res) => {
    Product.find()
        .exec((error, product) => {
            error ? res.status(500).json({ error: error }) : res.status(200).json(product)
        })
};

exports.getProduct = (req, res) => {
    Product.find({ _id: req.params.id }, function (err, docs) {
        if (err) res.json(err);
        else res.status(200).json(docs);
    });
}

exports.createProduct = (req, res, next) => {
    let image = req.file.path;
    let youtube_url = req.body.youtube_url;

    let youtube_url_embed = youtube_url.replace("watch?v=", "embed/");

    let product = new Product({
        _id: new mongodb.Types.ObjectId(),
        name: req.body.name,
        description: req.body.description,
        samarbetspartner: req.body.samarbetspartner,
        sektion: req.body.sektion,
        top: req.body.top,
        left: req.body.left,
        productImagesAndVideos: [
            {
                url: image,
                type: "image"
            },
            {
                url: youtube_url_embed,
                type: "video"
            }
        ]
    })

    product.save()
        .then((result) => {
            res.status(201).json(result);
        })
        .catch((err) => console.log(err))
};

exports.addImage = (req, res, next) => {
    Product.findByIdAndUpdate(req.body._id,
        { $push: { productImagesAndVideos: { url: req.file.path, type: "image" }} },
        { safe: true, new: true },
        function (err, doc) {
            if (err) {
                res.status(500).json(err);
            } else {
                res.status(200).json(doc)
            }
        }
    );
};

exports.removeImage = (req, res, next) => {
    Product.findByIdAndUpdate(req.body._id,
        { $pull: { productImagesAndVideos: { url: req.body.image_url, type: "image" }} },
        { safe: true, new: true },
        function (err, doc) {
            if (err) {
                res.status(500).json(err);
            } else {
                deleteFile(req.body.image_url)
                res.status(200).json(doc)
            }
        }
    );
};



exports.updateProduct = (req, res, next) => {
    Product.findOneAndUpdate({
        _id: req.body._id
    },
        {
            $set: {
                name: req.body.name,
                description: req.body.description,
                samarbetspartner: req.body.samarbetspartner,
                top: req.body.top,
                left: req.body.left,
            }
        }, { new: true }, function (err, doc) {
            err ? res.status(500).json({ error: err }) : res.status(200).json(doc)
        });
};

exports.deleteProducts = (req, res, next) => {
    Product.findByIdAndRemove({ _id: req.body._id }, (err, resp) => {
        if (err) {
            res.status(500).json({ error: err })
        } else {
            let imagesList = resp.productImagesAndVideos;
            for (let i = 0; i < imagesList.length; i++) {
                deleteFile(imagesList[i]);
            }
            res.status(204);
        }
    })
};
